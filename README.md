# ids721-indiv2

![pipeline status](https://gitlab.com/dukeaiml/IDS721/yx248-indiv2/badges/main/pipeline.svg)

## Create a Cargo Function Microservice Project

1. Install Rust and Cargo
Rust is a programming language that Cargo, its package manager, uses. To install Rust and Cargo on Ubuntu, open your terminal and run the following command:

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

This will download a script and start the installation. You'll need to follow the on-screen instructions.

2. Configure the Current Shell
After installation, configure your current shell by running:

```bash
source $HOME/.cargo/env
```

3. Create a New Cargo Project
To create a new Cargo project, use the cargo new command. For a microservice, you might want to start with a simple binary project:

```bash
cargo new yx248-indiv2 --bin
```

This command creates a new directory named my_microservice with the basic structure of a Rust project.

4. Add Dependencies
Edit the `Cargo.toml` file to add necessary dependencies. For a microservice, you might use frameworks like Actix-web, Rocket, or Warp. Here's an example using Actix-web:

```toml
axum = "0.7.4"
tokio = { version = "1.36.0", features = ["macros", "full"] }
tower = "0.4.13"
# s3
aws-config = "1.1.9"
aws-sdk-s3 = "1.21.0"
csv = "1.1"
# json
serde_json = "1.0.113"
serde = { version = "1.0.196", features = ["derive"] }
# openssl
openssl = { version = "0.10", features = ["vendored"] }
```

Always check for the latest version of these libraries.


## Add Your Own Functions for Microservice

1. Create a new file named `lib.rs` to hold your data processing functions and tests.

2. Modify your `main.rs` to fit your function requirements and your router structure.

- For my function, it is based on my mini-project2 function:
[yx248-mini2 Link](https://gitlab.com/dukeaiml/IDS721/yx248-mini2)


## Build and Run the Project Local

Navigate to your project directory in the terminal and run the project with Cargo:

```bash
cargo run
```

Your microservice should now be running on localhost at the specified port (e.g., 127.0.0.1:3000).


## Local Testing
Test your microservice by navigating to the address in a web browser or using tools like `curl`:

```bash
curl http://127.0.0.1:8080/
```


## Dockerfile

- Add `test` and `build` for the rust service to test whether the service get the correct result.
![Docker test and build code](images/dockerfile_test.png)

- If need to use the docker image to run the service, need to add `RUN cargo run` or if you have a Makefile you can add `RUN make run` in `Dockerfile`.

## Build Docker Container

- To build and run the Docker container without environment variables (e.g., for testing or development environments), use the following commands:

```bash
# Build the Docker image
sudo docker build -t yx248-indiv2-image .

# Run the container in detached mode (background) mapping port 3000
sudo docker run -dp 3000:3000 yx248-indiv2-image

# List running containers to find the container ID
sudo docker ps

# Stop the running container using its ID
sudo docker stop <container_id>

# Remove the stopped container by its ID
sudo docker rm <container_id>
```

Replace <container_id> with the actual ID of your Docker container, which you can find using sudo docker ps.

- If you need to include sensitive information or environment-specific variables (like AWS credentials) in your build, it's recommended to use a `docker-compose.yml` file along with a `.env` file. This approach keeps sensitive data out of the Dockerfile and image itself:

```bash
# Build the Docker image using docker-compose
sudo docker-compose build

# Run the container in detached mode (background) using docker-compose
sudo docker-compose up -d

# List running containers to find the container ID
sudo docker ps

# To stop and remove the running container, use docker-compose
sudo docker-compose down
```

- Here are the screenshot for my docker build, up, ps, and down operations:
![Docker Build](images/docker-compose_build.png)
![Docker Up Ps Down](images/docker-compose_up_ps_down.png)

## Running and Using the Micro Server

You have two options to run the microserver:
1. Using Docker: Deploy the microserver using the provided Docker image. (Ensure Docker is installed and running on your system.)
2. Build and Run with Cargo: Compile and start the server locally using Cargo with the command: `cargo run`
This command compiles the project and runs the resulting binary. The server will start listening on port 3000 by default.

### Usage

- Base URL:

```url
http://url:3000/
```

Use this URL to access the root of the micro server. This usually provides general information or a welcome message.

- Filtering Prices:

```url
http://url:3000/pricefilter/1/2
```

This URL demonstrates how to filter items by price range. Replace 1 and 2 with your desired lower and upper price bounds, respectively.

- Example Output
After accessing the endpoints, you should receive JSON responses based on the request parameters. Below is a screenshot showing an example result when running the microservice and accessing one of its routes:
![Docker Container Server Work Result](images/result_sample.png)


## Add cicd for Gitlab

- Integrating a CI/CD pipeline in GitLab allows us to automatically build, test, and validate our Dockerized project within the GitLab pipeline. By encapsulating all tests and tasks within the Dockerfile, we ensure that the pipeline executes these steps. If there are any issues with the code or the tasks fail, the pipeline halts and provides an error message, indicating the failure. This process ensures that any code changes are automatically tested for correctness before deployment, enhancing the project's reliability and streamlining development workflows. By incorporating all project testing and tasks directly within our Dockerfile, we've centralized the execution of our validation processes. This setup means that when the CI/CD pipeline is triggered, it needs to execute only a single stage: building and running the Docker container.
![cicd code](images/gitlab_cicd.png)

## Demo Video Link
[Demo Video Link](https://youtu.be/RnPc1ymnlRY)
